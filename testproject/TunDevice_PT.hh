#ifndef TunDevice__PT_HH
#define TunDevice__PT_HH

#include <TTCN3.hh>
#include "TunDevice_Types.hh"


// Note: Header file TunDevice_PortType.hh must not be included into this file!
// (because it includes this file)
// Please add the declarations of message types manually.

namespace TunDevice__PortType {

class TunDevice__PT_PROVIDER : public PORT {
public:
	TunDevice__PT_PROVIDER(const char *par_port_name);
	~TunDevice__PT_PROVIDER();

	void set_parameter(const char *parameter_name,
		const char *parameter_value);

private:
	/* void Handle_Fd_Event(int fd, boolean is_readable,
		boolean is_writable, boolean is_error); */
	void Handle_Fd_Event_Error(int fd);
	void Handle_Fd_Event_Writable(int fd);
	void Handle_Fd_Event_Readable(int fd);
	/* void Handle_Timeout(double time_since_last_call); */

	void open_device(void);
	void close_device(void);
	void log(const char *fmt, ...);
	void logHex(const char *prompt, const OCTETSTRING& msg);
	struct tun_data {
		int fd;
		char *dev;
	} tun_data;
	bool debugging;

protected:
	void user_map(const char *system_port);
	void user_unmap(const char *system_port);

	void user_start();
	void user_stop();

	void outgoing_send(const TunDevice__Types::Tun__send& send_par);
	virtual void incoming_message(const TunDevice__Types::Tun__recv& incoming_par) = 0;
};

} /* end of namespace */

#endif
